# AWS Golden Image

<!-- TOC depthFrom:1 -->

- [Introduction](#introduction)
- [Workflow](#workflow)
- [Installable modules](#installable-modules)
- [Preparation](#preparation)
- [Build](#build)


<!-- /TOC -->

## Introduction

**packer-ami-example** 프로젝트는 Packer를 이용 하여 AWS 클라우드에 최적화된 애플리케이션 서비스용 Golden 이미지(AMI: Amazon Machine Image)를 생성 하는 것을 목표로 합니다.

참고로, [Packer](https://www.packer.io/)는 Hashicorp 사에서 제공하는 이미지 프로비저닝 도구 입니다.

이미지는 Immutable 특성으로 설정의 변경이 없습니다. 이는 서비스 환경에 전달 될 때 애플리케이션 런-타임의 일관성을 보장 합니다. 
또한, 빠른 start-up 구성 하므로 [Cloud Native Application](https://tanzu.vmware.com/cloud-native) 을 구현하는데 중요한 역할을 합니다.

### Image Type
- [AMI](https://docs.aws.amazon.com/ko_kr/AWSEC2/latest/UserGuide/AMIs.html): EC2 와 같은 OS 레벨의 런-타임을 구성 하는 이미지 입니다.
- [Docker](https://docs.docker.com/get-started/overview/): 컨테이너 레벨의 런-타임을 구성 하는 이미지 입니다.

## Workflow

Packer를 통한 프로비저닝 과정은 다음과 같습니다.
1. Build: EC2 인스턴스를 생성 합니다.  
   (AMI 이미지를 bake 할때까지만 임시로 인스턴스를 올립니다.)
2. Provisioning: 애플리케이션 런타임 환경에 필요한 각종 패키지들을 설치 합니다.   
   (ssm-agent, code-deploy-agent, jdk, node 등을 필요에 따라 구성)
3. Post-Process: AMI 이미지가 생성된 이후 post-process 를 처리 합니다.  
   (불필요한 삭제 또는 AMI 이미지 메타 정보 로그 등.)
![workflow](./docs/images/packer-01.png)


## Installable modules

|     모듈     |            설명           |
|:-----------:|:--------------------------:|
| [aws-ssm-agent](https://docs.aws.amazon.com/systems-manager/latest/userguide/managed_instances.html)  | AWS EC2 컴퓨팅 리소스에 대해 체계적인 관리를 지원하는 에이전트 입니다.      |
| [aws-codedeploy-agent](https://docs.aws.amazon.com/codedeploy/latest/userguide/welcome.html)  | 애플리케이션의 지속적인 배포를 지원하는 AWS Code 시리즈 제품 입니다. |

필요에 따라 JDK, Node 등 애플리케이션에 꼭 필요한 모듈을 선택적으로 추가 할 수 있습니다. 



<!--
클라우드 애플리케이션 요소에서 베이스 이미지의 역할은 상당히 중요 합니다. 그 이유는 다음과 같습니다.  
- 이미지는 CD 와 같이 전달 과정에서 변경이 발생 하지 않는 Immutable 속성을 가집니다.
- 보다 빠른 애플리케이션의 개발과 구동 시간 최적화가 가능합니다.
- 일관된 애플리케이션 환경을 제공 합니다.
- 애플리케이션 런타임에 불필요한 SW 패키지의 제거하여 경량화된 OS 크기 최적화를 합니다.
- 버저닝을 통해 관리 될 수 있는 IaC 코드로 대체 됩니다. 
- 가장 안전한 OS 및 런타임 환경을 지속적으로 구성 관리 (Security Update & Patches) 합니다.
- 사람의 실수로 인한 애러를 사전에 방지 합니다.



-->

## Preparation
사전 준비 작업으로 다음과 같은 작업을 진행 합니다.
1. AWS IAM 계정을 생성하고 Access-Key 를 발급 받습니다.  
   [IAM 계정을 생성](https://docs.aws.amazon.com/ko_kr/IAM/latest/UserGuide/id_users_create.html#id_users_create_console) 가이드 참고
2. 발급 받은 [Access-Key](https://docs.aws.amazon.com/ko_kr/IAM/latest/UserGuide/id_credentials_access-keys.html) 를 통해 aws configure profile 을 구성 합니다.
3. packer 를 설치 합니다.
4. anible 을 설치 합니다.

### Aws Profile
AWS Access-Key(Access Key, Secret Access Key)  정보는 절대 노출되어선 안되는 중요한 보안 정보 입니다. 

그러므로 Access-Key 정보를 숨기고, AWS 자원을 액세스 하기 위해서 AWS 프로파일 정보를 설정 하여 진행 하는게 좋습니다.

예제는 'terra' 라는 프로파일을 구성하여 진행 합니다. 

```bash
aws configure --profile terra

AWS Access Key ID [None]: *********
AWS Secret Access Key [None]: *******
Default region name [None]: ap-northeast-2
Default output format [None]: json
```

### Installation packer

macOS 사용자는 brew 패키지 매지저를 통해 ansible 을 설치 할 수 있습니다.
```shell
brew install packer
```

### Installation ansible 
macOS 사용자는 brew 패키지 매지저를 통해 ansible 을 설치 할 수 있습니다.
```shell
brew update
brew install ansible
```



## Build
packer + ansible-playbook 을 통해 AMI 이미지를 빌드 합니다.

### checkout project
git 을 통해 프로젝트를 checkout 합니다.
```shell
mkdir ~/workspace
cd ~/workspace

git clone https://gitlab.com/symplesims/packer-ami-example.git
```
### Packer 템플릿 파일 검증 
템플릿 파일이 유효 한지는 packer validate 명령을 통해 확인 할 수 있습니다.

```shell
AWS_PROFILE=terra; AWS_REGION=ap-northeast-2; \               
packer validate ./packer/ubuntu/build.json
```

### Packer 템플릿을 통한 빌드

packer build 명령을 통해 AMI 이미지를 빌드 합니다.

```shell
cd ~/workspace/packer-ami-example

AWS_PROFILE=terra; AWS_REGION=ap-northeast-2; \               
packer build ./packer/ubuntu/build.json

```

### Packer 빌드 결과 확인

처리 과정은 [Provisioning 로그](./docs/build.out.md) 로 확인인 가능 하며, 빌드가 완료되면 AMI 아이디가 출력 됩니다.

```shell
amazon-ebs: AMI: ami-02e085bc686d3ad4b
```

방금 생성한 AMI 아이디( **ami-02e085bc686d3ad4b**) 를 AWS Console 화면에서 조회 하면 다음과 같이 생성된 AMI 이미지를 확인 할 수 있습니다.
![ami-02e085bc686d3ad4b](./docs/images/packer-02.png)

