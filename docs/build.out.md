## Provisioning 진행 과정 로그 확인
```shell
==> amazon-ebs: Connected to SSH!
==> amazon-ebs: Provisioning with shell script: /var/folders/87/3npljfbs579by16v8jjmfjy40000gn/T/packer-shell331886612
    amazon-ebs: Waiting for cloud-init
    amazon-ebs:  . .
==> amazon-ebs: Provisioning with shell script: ./packer/ubuntu/bootstrap.sh
==> amazon-ebs: ++ command -v yum
==> amazon-ebs: + [[ -x '' ]]
==> amazon-ebs: ++ command -v apt-get
==> amazon-ebs: + [[ -x /usr/bin/apt-get ]]
==> amazon-ebs: + ubuntuPackageManager
==> amazon-ebs: + sudo add-apt-repository universe
    amazon-ebs: 'universe' distribution component is already enabled for all sources.
==> amazon-ebs: + sudo apt-get update
    amazon-ebs: Hit:1 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu bionic InRelease
    amazon-ebs: Get:2 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu bionic-updates InRelease [88.7 kB]
    amazon-ebs: Get:3 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu bionic-backports InRelease [74.6 kB]
    amazon-ebs: Get:4 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu bionic/universe amd64 Packages [8570 kB]
    amazon-ebs: Get:5 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu bionic/universe Translation-en [4941 kB]
    amazon-ebs: Get:6 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu bionic/multiverse amd64 Packages [151 kB]
    amazon-ebs: Get:7 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu bionic/multiverse Translation-en [108 kB]
    amazon-ebs: Get:8 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu bionic-updates/main amd64 Packages [1755 kB]
    amazon-ebs: Get:9 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu bionic-updates/main Translation-en [372 kB]
    amazon-ebs: Get:10 http://ap-northeast-2.ec2.archive.ubuntu.com/ubuntu bionic-updates/restricted amd64 Packages [206 kB]
.
.
.
==> amazon-ebs: Creating AMI ubuntu-20-12-09_1246 from instance i-053f27349708b21d1
    amazon-ebs: AMI: ami-02e085bc686d3ad4b
==> amazon-ebs: Waiting for AMI to become ready...
==> amazon-ebs: Adding tags to AMI (ami-02e085bc686d3ad4b)...
==> amazon-ebs: Tagging snapshot: snap-0cadfafa6c8863da3
==> amazon-ebs: Creating AMI tags
    amazon-ebs: Adding tag: "Name": "ubuntu-20-12-09_1246"
    amazon-ebs: Adding tag: "Version": "snapshot-working"
    amazon-ebs: Adding tag: "OS_FAMILY": "Debian"
    amazon-ebs: Adding tag: "OS_VERSION": "18.04 LTS"
    amazon-ebs: Adding tag: "Builder": "packer"
==> amazon-ebs: Creating snapshot tags
==> amazon-ebs: Terminating the source AWS instance...
==> amazon-ebs: Cleaning up any extra volumes...
==> amazon-ebs: No volumes to clean up, skipping
==> amazon-ebs: Deleting temporary security group...
==> amazon-ebs: Deleting temporary keypair...
==> amazon-ebs: Running post-processor: manifest
Build 'amazon-ebs' finished after 4 minutes 21 seconds.

==> Wait completed after 4 minutes 21 seconds

==> Builds finished. The artifacts of successful builds are:
--> amazon-ebs: AMIs were created:
ap-northeast-2: ami-02e085bc686d3ad4b

--> amazon-ebs: AMIs were created:
ap-northeast-2: ami-02e085bc686d3ad4b

```